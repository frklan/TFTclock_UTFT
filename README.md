
# TFT Clock
A very basic clock running on a NodeMCU1.0 with a touch TFT display connected via SPI

Assumes development is done in [Platform IO](http://platformio.org).

## HW

- NodeMCU v1.0
- 2.8" TFT SPI display part no TJCTM24028-SPI

## Connections

![NodeMCU](http://arduino-project.net/wp-content/uploads/2016/03/NodeMcu-pinout.jpg)

|      NodeMCU      |  2.8" display  |
|-------------------|----------------|
| 3V3               | 1   VCC        |
| GND               | 2   GND        |
| D8 (GPIO15)       | 3   CS         |
| 3V3               | 4   RESET      |
| D3 (GPIO0)        | 5   DC         |
| D7 (GPIO13/HMOSI) | 6   SDI(MOSI)  |
| D5 (GPIO14/HSCLK) | 7   SCK        |
| SD3 (GPIO10/SDD3) | 8   LED        |
| D6 (GPIO12/HMISO) | 9   SDO(MISO)  |
| D5 (GPIO14/HSCLK) | 10  T_CLK      |
| D2 (GPIO4)        | 11  T_CS       |
| D7 (GPIO13/HMOSI) | 12  T_DIN      |
| D6 (GPIO12/HMISO) | 13  T_DO       |
| D1 (GPIO5)        | 14  T_IRQ      |

## Libraries

Of cource this project leans heavily on other peoples work, the following libs will install automagically on PlatformIO >= 3.0 or manually with ````pio lib install [ ID ]````

- [ 44  ] Time
- [ 567 ] WifiManager

The following libraries are not available from PlatformIO and needs to be local to the project (included in 'lib/' directory):

- [XPT2046](https://github.com/spapadim/XPT2046)
- [UTFT for ESP8266](https://github.com/gnulabis/UTFT-ESP8266)

## PCB

KiCad libs:

- [JCTM24028-SPI Display](https://github.com/sufzoli/suf-electronics-kicad-libraries>)
- NodeMcu1.0

The PCB layout is available in the pcb folder. N.B. there's an error on the ground plane and a jumper needs to be connected beteween pin 1 on R5 and pin 2 on R3.

Make sure ````LibDir```` in TFTclock_UTFT.pro points to the directory containing the above parts libraries before opening the PCB och schematics files in KiCad.

## License

Software released under GNU GPL Version 2

HW released under Open Source Hardware Version 1.0 [http://www.oshwa.org/definition/](http://www.oshwa.org/definition/)
