#include <Arduino.h>
#include <Time.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <SPI.h>
#include <UTFT.h>
#include <XPT2046.h>
#include "main.h"

#define ESP_SPI_FREQ 4000000

//#define TFT_LED_PIN 10 // GPIO10 = SD3
#define TFT_CS_PIN 15 // GPIO15 = D8
#define TFT_RST_PIN 0 // not uset, connect to 3.3V
#define TFT_SER_PIN 0 // GPIO0 = D3
UTFT tft(ILI9341_S5P, TFT_CS_PIN, TFT_RST_PIN, TFT_SER_PIN);

#define TOUCH_CS_PIN 4 // GPIO4 = D2
#define TOUCH_IRQ_PIN 5 // GPIO5 = D1
XPT2046 touch(TOUCH_CS_PIN, TOUCH_IRQ_PIN);

#define TFT_BRIGHTNESS_PIN 10
#define LIGHT_SENSOR_PIN A0

// Declare which fonts we will be using
extern uint8_t BigFont[];
extern uint8_t SmallFont[];
extern uint8_t SevenSegNumFontPlusPlus[];

// NTP stuff
static const char ntpServerName[] = "ntp1.sp.se";

WiFiUDP Udp;
unsigned int localPort = 8888;  // local port to listen for UDP packets

void setup()
{
  WiFiManager wifiManager;

  SPI.setFrequency(ESP_SPI_FREQ);

  //pinMode(LIGHT_SENSOR_PIN, INPUT);
  pinMode(TFT_BRIGHTNESS_PIN, OUTPUT);
  delay(1000);
  //digitalWrite(TFT_BRIGHTNESS_PIN, 0); // TODO: read light sensor and adjust accordingly.
  analogWrite(TFT_BRIGHTNESS_PIN, PWMRANGE / 2);

  tft.InitLCD(LANDSCAPE);
  tft.clrScr();

  tft.setBackColor(0, 0, 0);
  tft.setFont(BigFont);
  tft.setColor(VGA_RED);
  tft.print(F("-== TFT Clock ==-"), CENTER, 5);

  tft.setFont(SmallFont);
  tft.setColor(VGA_GRAY);
  tft.print(F(__TIMESTAMP__), RIGHT, 240 - tft.getFontYsize() * 2);
  tft.print(F("(C) 2016-2019, Fredrik Andersson"), RIGHT, 240 - tft.getFontYsize());

  tft.setColor(VGA_WHITE);
  tft.print(F("   Init touch panel"), LEFT, 30);
  touch.begin((uint16_t)tft.getDisplayYSize(), (uint16_t)tft.getDisplayXSize());
  touch.setCalibration(234, 1696, 1776, 267);
  touch.setRotation(touch.ROT270);
  tft.setColor(VGA_GREEN);
  tft.print(F("[OK]   "), RIGHT, 30);

  tft.setColor(VGA_WHITE);
  tft.print(F("   Starting serial"), LEFT, 45);
  Serial.begin(115200);
  while(!Serial);
  tft.setColor(VGA_GREEN);
  tft.print(F("[OK]   "), RIGHT, 45);

  tft.setColor(VGA_WHITE);
  tft.print(F("   Setting up WiFi"), LEFT, 60);
  wifiManager.autoConnect("TFTClock");
  WiFi.mode(WIFI_STA);
  tft.setColor(VGA_GREEN);
  tft.print(F("[OK]   "), RIGHT, 60);


  tft.setColor(VGA_WHITE);
  tft.print(F("   Starting NTP"), LEFT, 75);
  Udp.begin(localPort);
  tft.setColor(VGA_GREEN);
  tft.print(F("[OK]   "), RIGHT, 75);

  tft.setColor(VGA_WHITE);
  tft.print(F("   Syncing time"), LEFT, 90);
  setSyncProvider(getNtpTime);
  setSyncInterval(300);
  tft.setColor(VGA_GREEN);
  tft.print(F("[OK]   "), RIGHT, 90);

  delay(250);

  tft.clrScr();
  drawBorder(VGA_SILVER);
}


void loop()
{

  digitalClockDisplay();
  adjustScreenBrightness();


  // uint16_t x, y;
  // if (touch.isTouching())
  // {
  //   touch.getPosition(x, y);
  //   //Serial.println("Touching... x: "+ String(x) + ", y: " + String(y));
  //
  //   tft.drawPixel(x, y);
  //
  // }
}

void adjustScreenBrightness() {
  const unsigned int screenOn = 0;
  const unsigned int screenOff = PWMRANGE;
  const unsigned int screenTimeout = 22459;
  static unsigned long screenOnTimer = 0;

  if(hour() > 6 && hour() < 22) {
    analogWrite(TFT_BRIGHTNESS_PIN, screenOn);
    return;
  }

  if(touch.isTouching()) {
    screenOnTimer = millis() + screenTimeout;
    analogWrite(TFT_BRIGHTNESS_PIN, screenOn);
  }

  if(! (millis() < screenOnTimer)) {
    analogWrite(TFT_BRIGHTNESS_PIN, screenOff);
  }
}

void drawBorder(word color)
{
  word oldColor = tft.getColor();

  tft.setColor(color);
  tft.fillRect(0, 0, 320, 5); // top
  tft.fillRect(315, 0, 320, 240); // right
  tft.fillRect(0, 235, 320, 240); // bottom
  tft.fillRect(0, 0, 5, 240); // left

  tft.setColor(oldColor);
}


void digitalClockDisplay()
{
  static time_t prevDisplayTime = 0;
  static bool showDelim = true;
  char buff[12];
  uint8_t *oldFont;
  word oldColor;


  if (timeStatus() != timeNotSet && now() != prevDisplayTime)
  {

    oldFont = tft.getFont();
    oldColor = tft.getColor();
    tft.setColor(VGA_RED);

    if(showDelim)
    {
      sprintf(buff, "%02d:%02d:%02d", hour(), minute(), second());
      showDelim = false;
    }
    else
    {
      sprintf(buff, "%02d.%02d.%02d", hour(), minute(), second());
      showDelim = true;
    }

    tft.setFont(SevenSegNumFontPlusPlus);
    tft.print(buff, CENTER, 240 / 2 - tft.getFontYsize() / 2.2);

    tft.setFont(oldFont);
    tft.setColor(oldColor);

    prevDisplayTime = now();
  }
}


/*-------- NTP code ----------*/
#define NTP_PACKET_SIZE 48 // NTP time is in the first 48 bytes of message

time_t getNtpTime()
{
  byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming & outgoing packets
  IPAddress ntpServerIP; // NTP server's ip address

  while (Udp.parsePacket() > 0) ; // discard any previously received packets
  Serial.println(F("Transmit NTP Request"));
  // get a random server from the pool
  WiFi.hostByName(ntpServerName, ntpServerIP);
  Serial.print(ntpServerName);
  Serial.print(": ");
  Serial.println(ntpServerIP);
  sendNTPpacket(ntpServerIP);
  uint32_t beginWait = millis();
  while (millis() - beginWait < 1500)
  {
    int size = Udp.parsePacket();
    if (size >= NTP_PACKET_SIZE)
    {
      Serial.println(F("Receive NTP Response"));
      Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
      unsigned long secsSince1900;

      // convert four bytes starting at location 40 to a long integer
      secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
      secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
      secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
      secsSince1900 |= (unsigned long)packetBuffer[43];
      return secsSince1900 - 2208988800UL + getTimeZoneDstEurope(secsSince1900) * SECS_PER_HOUR;
    }
  }
  Serial.println(F("No NTP Response :-("));
  return 0; // return 0 if unable to get the time
}

// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress &address)
{
  byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming & outgoing packets

  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12] = 49;
  packetBuffer[13] = 0x4E;
  packetBuffer[14] = 49;
  packetBuffer[15] = 52;
  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();
}

int getTimeZoneDstEurope(time_t timeStamp)
{
  int timeZone;
  tmElements_t *tm = new tmElements_t;
  breakTime(timeStamp, *tm);

  uint16_t year = tm->Year + 1900; // tm-Year is only 8 bits..
  tm->Day++; // day start at 0 in NTP time?

  // last sunday of march
  int beginDSTDate =  (31 - (5 * year / 4 + 4) % 7);
  int beginDSTMonth = 3;
  int beginDSTHour = 2;

  //last sunday of october
  int endDSTDate = (31 - (5 * year /4 + 1) % 7);
  int endDSTMonth = 10;
  int endDSTHour = 3;

  // DST is valid as:
  if (((tm->Month > beginDSTMonth) && (tm->Month < endDSTMonth)) 
  || ((tm->Month == beginDSTMonth) && (tm->Day >= beginDSTDate) && (tm->Hour >= beginDSTHour)) 
  || ((tm->Month == endDSTMonth) && (tm->Day < endDSTDate)) 
  || ((tm->Month == endDSTMonth) && (tm->Day == endDSTDate) && tm->Hour <= (endDSTHour)))
  {
    timeZone = 2;  // DST europe = utc +2 hour
  }
  else
  {
    timeZone = 1; // nonDST europe = utc +1 hour
  }

  delete tm;
  return timeZone;
}
