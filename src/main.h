#include <Arduino.h>
#include <ESP8266WiFi.h>

void drawBorder(word color);
time_t getNtpTime();
void digitalClockDisplay();
void printDigits(int digits);
void sendNTPpacket(IPAddress &address);
void adjustScreenBrightness();
int getTimeZoneDstEurope(time_t timeStamp);
